#!/bin/bash

function tg_sendText() {
curl -s "https://api.telegram.org/bot$BOT_TOKEN/sendMessage" \
-d "parse_mode=html" \
-d text="${1}" \
-d chat_id=$CHAT_ID \
-d "disable_web_page_preview=true"
}

function tg_sendFile() {
curl -F chat_id=$CHAT_ID -F document=@${1} -F parse_mode=markdown https://api.telegram.org/bot$BOT_TOKEN/sendDocument
}

MANIFEST=git://github.com/crdroidandroid/android.git
BRANCH=11.0

mkdir -p /tmp/rom
cd /tmp/rom

# Repo init command, that -device,-mips,-darwin,-notdefault part will save you more time and storage to sync, add more according to your rom and choice. Optimization is welcomed! Let's make it quit, and with depth=1 so that no unnecessary things.
repo init --no-repo-verify --depth=1 -u "$MANIFEST" -b "$BRANCH" -g default,-device,-mips,-darwin,-notdefault

tg_sendText "Downloading sources"
# Sync source with -q, no need unnecessary messages, you can remove -q if want! try with -j30 first, if fails, it will try again with -j8
repo sync -c -q --force-sync --optimized-fetch --no-tags --no-clone-bundle --prune -j30 || repo sync -c --no-clone-bundle --no-tags --optimized-fetch --prune --force-sync -j8
rm -rf .repo

# Sync device tree and stuffs
git clone --depth=1 https://github.com/ps151/device device/xiaomi/miatoll
git clone --depth=1 https://github.com/ArrowOS-Devices/android_vendor_xiaomi_miatoll vendor/xiaomi/miatoll
git clone --depth=1 https://github.com/ArrowOS-Devices/android_device_xiaomi_sm6250-common device/xiaomi/sm6250-common
git clone --depth=1 https://github.com/ArrowOS-Devices/android_vendor_xiaomi_sm6250-common vendor/xiaomi/sm6250-common
git clone --depth=1 https://github.com/ArrowOS-Devices/android_kernel_xiaomi_sm6250 kernel/xiaomi/sm6250

# Normal build steps
. build/envsetup.sh
lunch lineage_miatoll-userdebug
export CCACHE_DIR=/tmp/ccache
export CCACHE_EXEC=$(which ccache)
export USE_CCACHE=1
ccache -M 20G
ccache -o compression=true
ccache -z

# upload function for uploading rom zip file! I don't want unwanted builds in my google drive haha!
up(){
	curl --upload-file $1 https://transfer.sh/ 
}
tg_sendText "Building"
mka api-stubs-docs
mka system-api-stubs-docs
mka test-api-stubs-docs
mka bacon -j16
up out/target/product/miatoll/*.zip
tg_sendText "download.txt"
up out/target/product/miatoll/*.json
tg_sendText "download.txt"
